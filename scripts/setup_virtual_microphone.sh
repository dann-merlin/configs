#!/bin/sh

virtual_mic_sink_name='virtual_microphone_sink'
virtual_mic_sink_display_name='Virtual_Microphone_Sink'

soundboard_sink_name='soundboard_sink'
soundboard_sink_display_name='Soundboard_Sink'

for sink in $(pactl list sinks short | cut -f 2); do
	if [ "${sink}" = "${virtual_mic_sink_name}" ]; then
		exit 0
	fi
done

# Create a soundboard sink, where the audio that should be mixed with the microphone is redirected to
pactl load-module module-null-sink sink_name="${soundboard_sink_name}" sink_properties=device.description="${soundboard_sink_display_name}"

# Create a virtual microphone sink where all audio intended for input is mixed
pactl load-module module-null-sink sink_name="${virtual_mic_sink_name}" sink_properties=device.description="${virtual_mic_sink_display_name}"

# Route the default microphone to the created microphone sink
microphone_source_name="alsa_input.usb-Samson_Technologies_Samson_Meteor_Mic-00.analog-stereo"
"$(pactl info | sed -En 's/Default Source: (.*)/\1/p')"
default_sink="$(pactl info | sed -En 's/Default Sink: (.*)/\1/p')"
pactl load-module module-loopback latency_msec=30 adjust_time=3 source="${microphone_source_name}" sink="${virtual_mic_sink_name}"
pactl set-default-source "${virtual_mic_sink_name}".monitor

# Route soundboard_sink monitor to virtual mic sink
pactl load-module module-loopback latency_msec=30 adjust_time=3 source="${soundboard_sink_name}".monitor sink="${virtual_mic_sink_name}"

# Also route it to the default sink, so you can hear it as well
pactl load-module module-loopback latency_msec=30 adjust_time=3 source="${soundboard_sink_name}".monitor sink="${default_sink}"
