#!/usr/bin/env python3
import subprocess as sp
import os
import csv

KEYBOARD_ID = 8
LOG_FILE = os.path.expanduser("~/.keyboard_log.csv")
logged_keys = {}


def read_log_file():
    logged_keys.clear()
    try:
        with open(LOG_FILE, newline='') as csvfile:
            keys_csv = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for key_info in keys_csv:
                logged_keys[key_info[0]] = int(key_info[1])
    except FileNotFoundError:
        print('Creating a new log file at: ' + LOG_FILE)


def write_log_file():
    try:
        with open(LOG_FILE, 'w', newline='') as csvfile:
            log_writer = csv.writer(
                csvfile,
                delimiter=' ',
                quotechar='|',
                quoting=csv.QUOTE_MINIMAL
            )
            for keycode in logged_keys:
                log_writer.writerow([keycode, logged_keys[keycode]])
    except KeyboardInterrupt:
        write_log_file()
        quit()


def log_key(keycode):
    try:
        logged_keys[keycode] += 1
    except KeyError:
        logged_keys[keycode] = 1


def start_logging():
    read_log_file()
    process = sp.Popen(["xinput", "test", str(KEYBOARD_ID)], stdout=sp.PIPE)
    while True:
        log_key(process.stdout.readline().split()[-1].decode('utf-8'))
        write_log_file()


if __name__ == "__main__":
    start_logging()
