#!/usr/bin/env python3

import sys
import time
import argparse
import multiprocessing
import setproctitle
from multiprocessing import connection as ipc
from playsound import playsound

file_mutex_path = 'discord-audio-clip-file-mutex'
stop_command = 'stop it. get some help.'

setproctitle.setproctitle('discord-audio-clip.py')

parser = argparse.ArgumentParser(
    description=
    'Play sound to output device until interrupted by another instance'
)
parser.add_argument(
    'path',
    nargs='?',
    default=None,
    help='The path to the audio file that should be played.'
)
parser.add_argument(
    '-s',
    '--stop',
    action='store_const',
    const=True,
    default=False
)
parser.add_argument('-o', '--output-device', nargs=1)
args = parser.parse_args()


def play():
    playsound(clip_to_play)
    try:
        with ipc.Client(file_mutex_path) as conn:
            conn.send(stop_command)
    except:
        pass
    if args.stop:
        exit(0)



if args.path is None and not args.stop:
    parser.print_usage()
    exit()

clip_to_play = args.path

if not args.stop:
    playing_process = multiprocessing.Process(target=play)
    playing_process.start()

time.sleep(0.2)

try:
    with ipc.Client(file_mutex_path) as conn:
        conn.send(stop_command)
except:
    pass
if args.stop:
    exit(0)

for i in range(0,10):
    try:
        with ipc.Listener(file_mutex_path) as srv:
            with srv.accept() as conn:
                if conn.recv() == stop_command:
                    playing_process.terminate()
                else:
                    print("wtf is this", file=sys.stderr)
    except OSError as e:
        if e.errno != 98:
            raise e
        time.sleep(0.01)
        continue
    break
