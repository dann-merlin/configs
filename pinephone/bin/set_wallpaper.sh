#!/bin/sh

WALLPAPER_SYMLINK="${HOME}/.wallpaper"
WALLPAPER_SYMLINK_PREGEN="${HOME}/.wallpaper-pregen"
WALLPAPERS_DIR="/sdcard/wallpapers"
WALLPAPER_BACKUP="/usr/share/sxmo/background.jpg"
XRESOURCES_TEMPLATE="${HOME}/configs/pinephone/Xresources.template"
CUSTOM_XRESOURCES="${HOME}/.cache/wal/custom.Xresources"

build_custom_Xresources() {
	rm -f "${CUSTOM_XRESOURCES}"
	source "${HOME}/.cache/wal/colors.sh"
	sed -e "s;##FOREGROUND##;${color7};g" -e "s;##ALT_FOREGROUND##;${color6};g" -e "s;##BACKGROUND##;${color0};g" -e "s;##ALT_BACKGROUND##;${color4};g" < "${XRESOURCES_TEMPLATE}" > "${CUSTOM_XRESOURCES}"
}

load_generated_wallpaper() {
	echo "Loading pre-generated theme..."

	wal -R -n
	build_custom_Xresources
	xrdb -merge "${CUSTOM_XRESOURCES}"
	# xdotool key --delay 50 --clearmodifiers "super+F5"

	mv "${WALLPAPER_SYMLINK_PREGEN}" "${WALLPAPER_SYMLINK}"
	feh --no-fehbg --bg-center "${WALLPAPER_SYMLINK}"

	pkill conky
	while pgrep conky; do
		sleep 1
	done
	conky -c /usr/share/sxmo/appcfg/conky24h.conf -d

	echo "Loaded pre-generated theme."
}

generate_new_wallpaper() {
	echo "Generating next theme..."
	wallpaper="${WALLPAPER_BACKUP}"
	if [ -d "${WALLPAPERS_DIR}" ]; then
		wallpaper="$(find "${WALLPAPERS_DIR}" -type f | shuf -n 1)"
	fi
	ln -sf "${wallpaper}" "${WALLPAPER_SYMLINK_PREGEN}"
	wal -c
	if ! wal -i "${WALLPAPER_SYMLINK_PREGEN}" --backend wal -n -e -s -t; then
		wal -c -i "${WALLPAPER_BACKUP}" --backend wal -n -e -s -t
		ln -sf "${WALLPAPER_BACKUP}" "${WALLPAPER_SYMLINK_PREGEN}"
	fi
	echo "Done generating next theme."
}

main() {
	load_generated_wallpaper

	generate_new_wallpaper
}

main

unset WALLPAPER_SYMLINK
unset WALLPAPERS_DIR
unset WALLPAPER_BACKUP
unset main
