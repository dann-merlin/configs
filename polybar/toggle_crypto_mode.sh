#!/bin/sh

if [ -f /tmp/cryptomode ]; then
	rm /tmp/cryptomode
else
	touch /tmp/cryptomode
fi

rm /tmp/last_crypto_update
